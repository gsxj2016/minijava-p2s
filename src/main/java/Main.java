import minijava.p2s.codegen.CodeGen;
import minijava.p2s.codegen.GenState;
import minijava.p2s.jtb.ParseException;
import minijava.p2s.jtb.PigletParser;
import minijava.p2s.jtb.Token;
import minijava.p2s.jtb.syntaxtree.Node;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Error: No input file.");
            System.exit(2);
        }

        try (InputStream in = new FileInputStream(args[0])) {
            Node root = new PigletParser(in).Goal();
            GenState state = new GenState();
            root.accept(new CodeGen(), state);
            System.out.print(state.code);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        } catch (ParseException e) {
            Token token = e.currentToken;
            System.err.printf("Syntax error at %d:%d.\n", token.beginLine, token.beginColumn);
            System.exit(1);
        }

    }
}
