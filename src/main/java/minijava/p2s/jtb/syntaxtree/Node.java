//
// Generated by JTB 1.3.2
//

package minijava.p2s.jtb.syntaxtree;

/**
 * The interface which all syntax tree classes must implement.
 */
public interface Node extends java.io.Serializable {
   public void accept(minijava.p2s.jtb.visitor.Visitor v);
   public <R,A> R accept(minijava.p2s.jtb.visitor.GJVisitor<R,A> v, A argu);
   public <R> R accept(minijava.p2s.jtb.visitor.GJNoArguVisitor<R> v);
   public <A> void accept(minijava.p2s.jtb.visitor.GJVoidVisitor<A> v, A argu);
}

