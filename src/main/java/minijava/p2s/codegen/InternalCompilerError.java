package minijava.p2s.codegen;

class InternalCompilerError extends Error {
    InternalCompilerError(String message) {
        super("[BUG] " + message);
    }
}
