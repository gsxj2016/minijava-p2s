package minijava.p2s.codegen;

import minijava.p2s.jtb.syntaxtree.*;
import minijava.p2s.jtb.visitor.GJVoidDepthFirst;

import java.util.ArrayList;
import java.util.List;

public class CodeGen extends GJVoidDepthFirst<GenState> {
    /**
     * f0 -> "MAIN"
     * f1 -> StmtList()
     * f2 -> "END"
     * f3 -> ( Procedure() )*
     * f4 -> <EOF>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Goal n, GenState argu) {
        argu.generateCode("MAIN");
        ++argu.indentDepth;
        argu.enterProcedure();
        n.f1.accept(this, argu);
        --argu.indentDepth;
        argu.generateCode("END");
        argu.generateCode("");
        n.f3.accept(this, argu);
    }

    /**
     * f0 -> Label()
     * f1 -> "["
     * f2 -> IntegerLiteral()
     * f3 -> "]"
     * f4 -> StmtExp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Procedure n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        argu.generateCodeFragment(n.f0.f0.tokenImage);
        argu.generateCodeFragment("[");
        argu.generateCodeFragment(n.f2.f0.tokenImage);
        argu.generateCode("]");

        int params = Integer.parseInt(n.f2.f0.tokenImage);
        argu.enterProcedure();
        for (int i = 0; i < params; ++i) {
            if (argu.getOriginalTemp(i) != i)
                throw new InternalCompilerError(
                        "Param temp id mismatch, this should not happen.");
        }

        n.f4.accept(this, argu);
        argu.generateCode("");
    }

    /**
     * f0 -> "NOOP"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(NoOpStmt n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        argu.generateCode("NOOP");
    }

    /**
     * f0 -> "ERROR"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ErrorStmt n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        argu.generateCode("ERROR");
    }

    /**
     * f0 -> "CJUMP"
     * f1 -> Exp()
     * f2 -> Label()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(CJumpStmt n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        // exp to stat
        String target = argu.genExpCode(n.f1, this, 0);
        // generate code
        argu.generateCodeFragment("CJUMP");
        argu.generateCodeFragment(target);
        argu.generateCode(n.f2.f0.tokenImage);
    }

    /**
     * f0 -> "JUMP"
     * f1 -> Label()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(JumpStmt n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        argu.generateCodeFragment("JUMP");
        argu.generateCode(n.f1.f0.tokenImage);
    }

    /**
     * f0 -> "HSTORE"
     * f1 -> Exp()
     * f2 -> IntegerLiteral()
     * f3 -> Exp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(HStoreStmt n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        String addr = argu.genExpCode(n.f1, this, 0);
        String offset = n.f2.f0.tokenImage;
        String value = argu.genExpCode(n.f3, this, 0);
        argu.generateCodeFragment("HSTORE");
        argu.generateCodeFragment(addr);
        argu.generateCodeFragment(offset);
        argu.generateCode(value);
    }

    /**
     * f0 -> "HLOAD"
     * f1 -> Temp()
     * f2 -> Exp()
     * f3 -> IntegerLiteral()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(HLoadStmt n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        String reg = argu.genExpCode(n.f1, this, 0);
        String addr = argu.genExpCode(n.f2, this, 0);
        String offset = n.f3.f0.tokenImage;
        argu.generateCodeFragment("HLOAD");
        argu.generateCodeFragment(reg);
        argu.generateCodeFragment(addr);
        argu.generateCode(offset);
    }

    /**
     * f0 -> "MOVE"
     * f1 -> Temp()
     * f2 -> Exp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MoveStmt n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        String reg = argu.genExpCode(n.f1, this, 0);
        String value = argu.genExpCode(n.f2, this, 2);
        argu.generateCodeFragment("MOVE");
        argu.generateCodeFragment(reg);
        argu.generateCode(value);
    }

    /**
     * f0 -> "PRINT"
     * f1 -> Exp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(PrintStmt n, GenState argu) {
        if (argu.isExpression())
            throw new InternalCompilerError(
                    "expected expression, but found statement");

        String value = argu.genExpCode(n.f1, this, 1);
        argu.generateCodeFragment("PRINT");
        argu.generateCode(value);
    }

    /**
     * f0 -> "BEGIN"
     * f1 -> StmtList()
     * f2 -> "RETURN"
     * f3 -> Exp()
     * f4 -> "END"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(StmtExp n, GenState argu) {
        if (argu.isExpression()) {
            int retAllowedType = argu.getAllowedType();
            n.f1.accept(this, argu);
            String retValue = argu.genExpCode(n.f3, this, retAllowedType);
            argu.genResultExp(retValue, true);
        } else {
            argu.generateCode("BEGIN");
            ++argu.indentDepth;
            n.f1.accept(this, argu);
            String retValue = argu.genExpCode(n.f3, this, 1);
            argu.generateCodeFragment("RETURN");
            argu.generateCode(retValue);
            --argu.indentDepth;
            argu.generateCode("END");
        }
    }

    /**
     * f0 -> "CALL"
     * f1 -> Exp()
     * f2 -> "("
     * f3 -> ( Exp() )*
     * f4 -> ")"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Call n, GenState argu) {
        int allowedType = argu.getAllowedType();
        String func = argu.genExpCode(n.f1, this, 1);
        List<String> params = new ArrayList<>();
        n.f3.nodes.forEach(pn -> {
            String ps = argu.genExpCode(pn, this, 0);
            params.add(ps);
        });

        String callExpr = "CALL " + func
                + " ( "
                + String.join(" ", params)
                + " )";

        argu.genResultExp(callExpr, allowedType >= 2);
    }

    /**
     * f0 -> "HALLOCATE"
     * f1 -> Exp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(HAllocate n, GenState argu) {
        int allowedType = argu.getAllowedType();
        String sz = argu.genExpCode(n.f1, this, 0);
        String hExpr = "HALLOCATE " + sz;
        argu.genResultExp(hExpr, allowedType >= 2);
    }

    /**
     * f0 -> Operator()
     * f1 -> Exp()
     * f2 -> Exp()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(BinOp n, GenState argu) {
        int allowedType = argu.getAllowedType();
        n.f0.accept(this, argu);
        String op = n.f0.f0.choice.toString();
        String lhs = argu.genExpCode(n.f1, this, 0);
        String rhs = argu.genExpCode(n.f2, this, 1);
        String binExpr = op + " " + lhs + " " + rhs;
        argu.genResultExp(binExpr, allowedType >= 2);
    }

    /**
     * f0 -> "TEMP"
     * f1 -> IntegerLiteral()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Temp n, GenState argu) {
        argu.getAllowedType();
        int tempId = argu.getOriginalTemp(Integer.parseInt(n.f1.f0.tokenImage));
        argu.genResultExp("TEMP " + tempId, true);
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(IntegerLiteral n, GenState argu) {
        int allowedType = argu.getAllowedType();
        argu.genResultExp(n.f0.tokenImage, allowedType >= 1);
    }

    /**
     * f0 -> <IDENTIFIER>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Label n, GenState argu) {
        if (argu.isExpression()) {
            int allowedType = argu.getAllowedType();
            argu.genResultExp(n.f0.tokenImage, allowedType >= 1);
        } else {
            argu.generateCode(n.f0.tokenImage + "   NOOP");
        }
    }
}
