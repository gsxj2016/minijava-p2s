package minijava.p2s.codegen;

import minijava.p2s.jtb.syntaxtree.Node;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class GenState {
    private int nextTemp = 0;
    private Map<Integer, Integer> tempMap = new HashMap<>();

    private String lastExp = null;
    private int expressionType = -1;

    int indentDepth = 0;
    private boolean hideIndent = false;
    public StringBuilder code = new StringBuilder();

    /* Generate Code */

    private void generateIndent() {
        if (!hideIndent) {
            IntStream.range(0, indentDepth).forEach(x -> code.append("    "));
        }
    }

    void generateCodeFragment(String codeText) {
        generateIndent();
        code.append(codeText);
        code.append(' ');
        hideIndent = true;
    }

    void generateCode(String codeText) {
        generateIndent();
        code.append(codeText);
        code.append('\n');
        hideIndent = false;
    }

    /* Local variable management */

    void enterProcedure() {
        nextTemp = 0;
        tempMap.clear();
    }

    private int getNextTemp() {
        return nextTemp++;
    }

    int getOriginalTemp(int id) {
        return tempMap.computeIfAbsent(id, k -> getNextTemp());
    }

    private String getPrevExp() {
        if (lastExp == null)
            throw new InternalCompilerError(
                    "missing expression");
        String result = lastExp;
        lastExp = null;
        return result;
    }

    /* Expressions */

    private String newTemp() {
        return "TEMP " + getNextTemp();
    }

    int getAllowedType() {
        if (expressionType == -1)
            throw new InternalCompilerError(
                    "empty exp type");
        int result = expressionType;
        expressionType = -1;
        return result;
    }

    boolean isExpression() {
        return expressionType != -1;
    }

    private void setExpressionType(int type) {
        if (expressionType != -1)
            throw new InternalCompilerError(
                    "set exp type twice");
        expressionType = type;
    }

    String genExpCode(Node node, CodeGen gen, int type) {
        setExpressionType(type);
        node.accept(gen, this);
        return getPrevExp();
    }

    private void setExpression(String exp) {
        if (lastExp != null)
            throw new InternalCompilerError(
                    "set exp twice");
        lastExp = exp;
    }

    void genResultExp(String src, boolean noTemp) {
        if (noTemp) {
            setExpression(src);
        } else {
            String resultTemp = newTemp();
            generateCodeFragment("MOVE");
            generateCodeFragment(resultTemp);
            generateCode(src);
            setExpression(resultTemp);
        }
    }
}
