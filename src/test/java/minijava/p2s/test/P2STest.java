package minijava.p2s.test;

import minijava.p2s.codegen.CodeGen;
import minijava.p2s.codegen.GenState;
import minijava.p2s.jtb.ParseException;
import minijava.p2s.jtb.PigletParser;
import minijava.p2s.jtb.syntaxtree.Node;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.stream.Collectors;

public class P2STest {
    private File pgi;
    private File spp;

    public P2STest() throws IOException {
        pgi = File.createTempFile("pgi", "jar");
        spp = File.createTempFile("spp", "jar");
        pgi.deleteOnExit();
        spp.deleteOnExit();
        try (InputStream pgiRes = getClass().getResourceAsStream("/pgi.jar")) {
            Files.copy(pgiRes, pgi.getAbsoluteFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        try (InputStream sppRes = getClass().getResourceAsStream("/spp.jar")) {
            Files.copy(sppRes, spp.getAbsoluteFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        System.out.printf("Piglet interpreter: %s\n", pgi.getAbsolutePath());
        System.out.printf("SPiglet parser: %s\n", spp.getAbsolutePath());
    }

    private String generateSPiglet(String resName) throws IOException, ParseException {
        try (InputStream in = getClass().getResourceAsStream(resName)) {
            Node root = new PigletParser(in).Goal();
            GenState state = new GenState();
            root.accept(new CodeGen(), state);
            return state.code.toString();
        }
    }

    private String getPiglet(String resName) throws IOException {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(getClass().getResourceAsStream(resName)))) {
            return reader.lines().collect(Collectors.joining("\n"));
        }
    }

    private String runInterpreter(String code) throws IOException {
        System.out.print(" ** code len ");
        System.out.println(code.length());
        Process process = Runtime.getRuntime().exec(new String[]{"java", "-jar", pgi.getAbsolutePath()});
        PrintWriter writer = new PrintWriter(process.getOutputStream());
        writer.print(code);
        writer.close();
        process.getErrorStream().close();

        String output;
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {
            output = reader.lines().collect(Collectors.joining("\n"));
        }

        while (process.isAlive()) {
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return output;
    }

    private boolean runParser(String code) throws IOException {
        System.out.print(" ** code len ");
        System.out.println(code.length());
        Process process = Runtime.getRuntime().exec(new String[]{"java", "-jar", spp.getAbsolutePath()});
        PrintWriter writer = new PrintWriter(process.getOutputStream());
        writer.print(code);
        writer.close();
        process.getErrorStream().close();
        process.getInputStream().close();

        while (process.isAlive()) {
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return process.exitValue() == 0;
    }

    private void testFile(String resName) throws IOException, ParseException {
        System.out.print("Testing with file: ");
        System.out.println(resName);
        String pCode = getPiglet(resName);
        String sCode = generateSPiglet(resName);
        Assert.assertNotNull(pCode);
        Assert.assertNotNull(sCode);
        System.out.println(" * check s");
        Assert.assertTrue(runParser(sCode));
        System.out.println(" * run p");
        String ans = runInterpreter(pCode);
        System.out.println(" * run s");
        String out = runInterpreter(sCode);
        if (ans.equals(out)) {
            System.out.println("Output is identical.");
        } else {
            System.out.println("## OUTPUT");
            System.out.println(out);
            System.out.println("## EXPECTED");
            System.out.println(ans);
            System.out.println("########");
            Assert.fail();
        }
        System.out.println();
    }

    @Test
    public void uclaTestCases() throws IOException, ParseException {
        testFile("/test_ucla/QuickSort.pg");
        testFile("/test_ucla/MoreThan4.pg");
        testFile("/test_ucla/BinaryTree.pg");
        testFile("/test_ucla/BubbleSort.pg");
        testFile("/test_ucla/TreeVisitor.pg");
        testFile("/test_ucla/LinearSearch.pg");
        testFile("/test_ucla/LinkedList.pg");
        testFile("/test_ucla/Factorial.pg");
    }

    @Test
    public void importedTestCases() throws IOException, ParseException {
        testFile("/test_j2p2s/QuickSort.pg");
        testFile("/test_j2p2s/MoreThan4.pg");
        testFile("/test_j2p2s/BinaryTree.pg");
        testFile("/test_j2p2s/BubbleSort.pg");
        testFile("/test_j2p2s/TreeVisitor.pg");
        testFile("/test_j2p2s/LinearSearch.pg");
        testFile("/test_j2p2s/LinkedList.pg");
        testFile("/test_j2p2s/Factorial.pg");
        testFile("/test_j2p2s/1-PrintLiteral.pg");
        testFile("/test_j2p2s/2-Add.pg");
        testFile("/test_j2p2s/3-Call.pg");
        testFile("/test_j2p2s/4-Vars.pg");
        testFile("/test_j2p2s/5-OutOfBounds.pg");
        testFile("/test_j2p2s/array.pg");
        testFile("/test_j2p2s/arrayOut.pg");
        testFile("/test_j2p2s/arrayOut2.pg");
        testFile("/test_j2p2s/arrayRef.pg");
        testFile("/test_j2p2s/arrayNull.pg");
        testFile("/test_j2p2s/arrayFetchNull.pg");
        testFile("/test_j2p2s/arrayLenNull.pg");
        testFile("/test_j2p2s/arrayNeg.pg");
        testFile("/test_j2p2s/arrayNeg2.pg");
        testFile("/test_j2p2s/zeroLenArray.pg");
        testFile("/test_j2p2s/badArrayAlloc.pg");
        testFile("/test_j2p2s/nestedCall.pg");
        testFile("/test_j2p2s/emptyClass.pg");
        testFile("/test_j2p2s/calc.pg");
        testFile("/test_j2p2s/override.pg");
        testFile("/test_j2p2s/callNull.pg");
        testFile("/test_j2p2s/shortcut.pg");
        testFile("/test_j2p2s/evalSeq.pg");
        testFile("/test_j2p2s/loop.pg");
    }

    @Test
    public void myTestCases() throws IOException, ParseException {
        testFile("/test_my/double_label.pg");
    }

}
